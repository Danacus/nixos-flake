{ lib, config, pkgs, ... }:
let
  netcfg = config.os.network;
  domain = netcfg.domain;
  proxyBase = locations: {
    inherit locations;

    forceSSL = true;
    useACMEHost = domain;

    extraConfig = ''
      proxy_buffer_size 8k;
    '';
  };
  proxy = host: port:
    proxyBase {
      "/" = {
        proxyPass = "http://${host}:" + toString (port) + "/";
        proxyWebsockets = true;
      };
    };

  mapHosts = services:
    lib.mapAttrs' (name: value:
      lib.nameValuePair "${value.endpoint}.${domain}"
      (proxy value.host value.port)) services;
  externalHosts =
    mapHosts (lib.filterAttrs (name: value: value.external) netcfg.services);
  internalHosts =
    mapHosts (lib.filterAttrs (name: value: !value.external) netcfg.services);
in {
  services.nginx = {
    enable = true;

    logError = "stderr debug";

    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedTlsSettings = true;
    recommendedProxySettings = true;

    resolver.addresses = [ "10.0.1.1" "[fdac:aba8:b3ae:10::1]:5353" ];

    virtualHosts = externalHosts // internalHosts;
  };

  users.users.nginx.extraGroups = [ "acme" ];

  security.acme = {
    acceptTerms = true;
    defaults.email = "daan@vanoverloop.xyz";
    certs."${domain}" = {
      dnsProvider = "cloudflare";

      # Use cloudflare to resolve DNS, not the local DNS server
      dnsResolver = "1.1.1.1:53";

      environmentFile = "/var/lib/acme_cloudflare";

      extraDomainNames = [ "*.${domain}" ];
    };
  };

  services.oauth2_proxy = {
    enable = true;
    provider = "keycloak-oidc";
    clientID = "oauth2-proxy";
    keyFile = "/var/lib/oauth2_proxy_secret";
    reverseProxy = true;
    email.domains = [ "*" ];
    requestLogging = true;
    cookie = { domain = ".vanoverloop.xyz"; };
    httpAddress = "127.0.0.1:4180";
    upstream = "http://127.0.0.1:5180/";
    scope = "openid email";

    extraConfig = {
      oidc-issuer-url = "https://auth.vanoverloop.xyz/realms/master";
      allowed-role = "home";
      skip-provider-button = true;
      session-cookie-minimal = true;
      session-store-type = "redis";
      redis-connection-url = "redis://127.0.0.1:31638";
      whitelist-domain = ".vanoverloop.xyz";
    };

    nginx = {
      proxy = "http://127.0.0.1:4180";
      virtualHosts = builtins.attrNames internalHosts;
    };
  };

  services.redis.servers.oauth2_proxy = {
    enable = true;
    bind = "127.0.0.1";
    port = 31638;
  };

  services.dnsmasq = {
    enable = true;
    resolveLocalQueries = true;
    settings = { address = [ "/${domain}/127.0.0.1" ]; };
  };
}
