{
  config,
  pkgs,
  lib,
  ...
}:
{
  services.xserver.enable = true;
  services.displayManager.sddm.enable = true;
  services.desktopManager.plasma6.enable = true;

  programs.ssh.askPassword = lib.mkForce "${pkgs.gnome.seahorse}/libexec/seahorse/ssh-askpass";
  services.xserver.displayManager.gdm.enable = lib.mkForce false;
}
