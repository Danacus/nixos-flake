{
  lib,
  config,
  pkgs,
  ...
}:
{
  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud29;
    hostName = "cloud.vanoverloop.xyz";
    database.createLocally = true;
    config = {
      dbtype = "pgsql";
      dbtableprefix = "oc_";
      dbname = "nextcloud";
      adminpassFile = "/var/lib/nextcloud_admin_password";
    };
    caching = {
      redis = true;
      memcached = true;
    };
    configureRedis = true;
    settings = {
      trusted_proxies = [
        "localhost"
        "10.0.3.1"
      ];
      trusted_domains = [
        "localhost"
        "nextcloud.lan"
        "nas.lan"
      ];
      ldapProviderFactory = "OCA\\User_LDAP\\LDAPProviderFactory";
      oidc_login_client_id = "nextcloud";
      # TODO: Hide this secret :)
      oidc_login_client_secret = "HWJ3dG809ZqwOUFvHJHRI0BMlWo8Ak50";
      oidc_login_provider_url = "https://auth.vanoverloop.xyz/realms/master";
      oidc_login_end_session_redirect = "true";
      oidc_login_logout_url = "https://cloud.vanoverloop.xyz/apps/oidc_login/oidc";
      oidc_login_auto_redirect = "false";
      oidc_login_redir_fallback = "true";
      oidc_login_attributes = {
        id = "preferred_username";
        mail = "email";
        ldap_uid = "preferred_username";
      };
      oidc_login_proxy_ldap = "true";
      oidc_login_button_text = "Log in with Keycloak";
      enable_previews = "false";
    };
  };

  services.nginx.virtualHosts."cloud.vanoverloop.xyz".listen = [
    {
      addr = "0.0.0.0";
      port = 8649;
    }
  ];

  users.groups.home.members = [ "nextcloud" ];

  networking.firewall.allowedTCPPorts = [ 8649 ];
}
