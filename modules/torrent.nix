{
  lib,
  config,
  pkgs,
  ...
}:
let
  ns = "torrentns";
  NetworkNamespacePath = "/var/run/netns/${ns}";
  internalIp = "10.255.255.2";
  hostIp = "10.255.255.1";
  netmask = "24";
  srv = config.os.network.services;
  torrentServices = with srv; [
    qbittorrent
    radarr
    sonarr
    prowlarr
  ];
in
{
  imports = [ ./qbittorrent.nix ];

  services.qbittorrent = {
    enable = true;
    group = "home";
    openFirewall = true;
    port = srv.qbittorrent.port;
  };

  nixpkgs.config.permittedInsecurePackages = [
    "aspnetcore-runtime-6.0.36"
    "aspnetcore-runtime-wrapped-6.0.36"
    "dotnet-sdk-6.0.428"
    "dotnet-sdk-wrapped-6.0.428"
  ];

  services.sonarr = {
    enable = true;
    group = "home";
    openFirewall = true;
  };

  services.radarr = {
    enable = true;
    group = "home";
    openFirewall = true;
  };

  services.prowlarr = {
    enable = true;
    openFirewall = true;
  };

  systemd.services =
    let
      serviceConfigCommon = {
        inherit NetworkNamespacePath;
        RuntimeMaxSec = lib.mkForce "1d";
        Restart = lib.mkForce "always";
      };
    in
    {
      torrentns = {
        description = "Torrenting network namespace";
        wantedBy = [ "default.target" ];
        before = [ "network.target" ];
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
          ExecStart = "${pkgs.iproute2}/bin/ip netns add torrentns";
          ExecStop = "${pkgs.iproute2}/bin/ip netns del torrentns";
        };
      };

      torrentns-interfaces = {
        description = "Torrenting network interfaces";
        wantedBy = [ "default.target" ];
        requires = [ "torrentns.service" ];
        after = [ "torrentns.service" ];
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
          ExecStart = pkgs.writeScript "routeup" ''
            #! ${pkgs.bash}/bin/bash
            ${pkgs.iproute2}/bin/ip link add veth0 type veth peer name veth1
            ${pkgs.iproute2}/bin/ip link set veth1 netns torrentns

            ${pkgs.iproute2}/bin/ip addr add ${hostIp}/${netmask} dev veth0
            ${pkgs.iproute2}/bin/ip link set dev veth0 up

            ${pkgs.iproute2}/bin/ip netns exec torrentns ${pkgs.iproute2}/bin/ip addr add ${internalIp}/${netmask} dev veth1
            ${pkgs.iproute2}/bin/ip netns exec torrentns ${pkgs.iproute2}/bin/ip link set dev veth1 up
            ${pkgs.iproute2}/bin/ip netns exec torrentns ${pkgs.iproute2}/bin/ip ro add default via ${hostIp}
            ${pkgs.iproute2}/bin/ip netns exec torrentns ${pkgs.iproute2}/bin/ip link set dev lo up
          '';
          ExecStop = pkgs.writeScript "routedown" ''
            #! ${pkgs.bash}/bin/bash
            ${pkgs.iproute2}/bin/ip link del veth0
            ${pkgs.iproute2}/bin/ip netns exec torrentns ${pkgs.iproute2}/bin/ip link del veth1
          '';
        };
      };

      qbittorrent.serviceConfig = serviceConfigCommon;
      sonarr.serviceConfig = serviceConfigCommon;
      radarr.serviceConfig = serviceConfigCommon;
      prowlarr.serviceConfig = serviceConfigCommon;
    };

  networking.wireguard.interfaces = {
    air = {
      interfaceNamespace = ns;
      privateKeyFile = "/root/wireguard-keys/privatekey-air";
      ips = [
        "10.131.60.172/32"
        "fd7d:76ee:e68f:a993:71a5:4c6:1a5e:51b2/128"
      ];
      mtu = 1320;

      peers = [
        {
          publicKey = "PyLCXAQT8KkM4T+dUsOQfn+Ub3pGxfGlxkIApuig+hk=";
          presharedKeyFile = "/root/wireguard-keys/psk-air";
          allowedIPs = [
            "0.0.0.0/0"
            "0::/0"
          ];
          endpoint = "europe3.vpn.airdns.org:1637";
          persistentKeepalive = 15;
        }
      ];
    };
  };

  # I'm too stupid for routing tables, so I'll use a proxy
  services.nginx =
    let
      torrentHost = port: {
        listen = [
          {
            addr = "0.0.0.0";
            inherit port;
          }
        ];
        locations."/" = {
          proxyPass = "http://${internalIp}:${toString port}";
          proxyWebsockets = true; # needed if you need to use WebSocket
        };
      };
    in
    {
      enable = true;
      virtualHosts = builtins.listToAttrs (
        map (value: {
          name = value.endpoint;
          value = torrentHost value.port;
        }) torrentServices
      );
    };

  networking.firewall.allowedTCPPorts = map (s: s.port) torrentServices;
}
