{ ... }:
{
  # Mount NFS devices
  fileSystems."/nas/media" = {
    device = "truenas.lan:/mnt/tank/media";
    fsType = "nfs";
    options = [
      "x-systemd.automount"
      "x-systemd.mount-timeout=30"
      "_netdev"
    ];
  };

  fileSystems."/nas/files" = {
    device = "truenas.lan:/mnt/tank/files";
    fsType = "nfs";
    options = [
      "x-systemd.automount"
      "x-systemd.mount-timeout=30"
      "_netdev"
    ];
  };

  fileSystems."/mnt/media" = {
    device = "mini.lan:/mnt/data/media";
    fsType = "nfs";
    options = [
      "x-systemd.automount"
      "x-systemd.mount-timeout=30"
      "_netdev"
    ];
  };
}
