{ config, pkgs, ... }:
{
  fonts = {
    fontDir.enable = true;
    enableDefaultPackages = true;
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji

      fira-code
      fira-code-symbols

      fira
    ];

    fontconfig = {
      defaultFonts = {
        serif = [ "Noto Serif" ];
        sansSerif = [ "Noto Sans" ];
        monospace = [ "Fira Code" ];
      };
    };
  };

  security.polkit.enable = true;

  services.flatpak.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  # services.desktopManager.plasma6.enable = true;
  # programs.ssh.askPassword =
  #   pkgs.lib.mkForce "${pkgs.kdePackages.ksshaskpass.out}/bin/ksshaskpass";

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    options = "eurosign:e,caps:escape";
  };

  # i18n.inputMethod = {
  #   enable = true;
  #   type = "ibus";
  #   ibus.engines = with pkgs.ibus-engines; [ anthy ];
  # };

  environment.variables = {
    IBUS_ENABLE_SYNC_MODE = "1";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.gutenprint ];

  # Disabled to mitigate against vulnerability
  # services.avahi.enable = true;
  # services.avahi.nssmdns4 = true;

  # for a WiFi printer
  services.avahi.openFirewall = true;

  hardware.bluetooth.enable = true;

  programs.gnupg.agent = {
    enable = true;
    # pinentryFlavor = "gnome3";
    pinentryPackage = pkgs.pinentry-gnome3;
    enableSSHSupport = true;
  };

  services.pcscd.enable = true;

  # Enable sound.
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
  };

  programs.kdeconnect = {
    enable = true;
    # package = pkgs.gnomeExtensions.gsconnect;
  };

  programs.firefox = {
    enable = true;
  };

  environment.systemPackages = with pkgs; [
    gnome-software
    signal-desktop
    pinentry
    wl-clipboard
    linux-wifi-hotspot
    eid-mw
    pcsctools
    scrcpy
    # (pkgs.callPackage ./ffsubsync.nix { })
    pdfpc
    appimage-run
    piper
  ];

  services.ratbagd.enable = true;

  hardware.steam-hardware.enable = true;

  hardware.sane.enable = true;

  hardware.graphics.enable = true;

  programs.adb.enable = true;

  # Enable binfmt emulation of aarch64-linux.
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  services.earlyoom = {
    enable = true;
    enableNotifications = true;
  };
}
