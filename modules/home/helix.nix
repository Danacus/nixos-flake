{ pkgs, ... }:
{
  programs.helix = {
    enable = true;
    settings = {
      theme = "onedark";
      editor = {
        bufferline = "multiple";
        color-modes = true;
        line-number = "relative";
        cursor-shape.insert = "bar";
        soft-wrap.enable = true;
        idle-timeout = 0;
      };
      keys.normal."+" = {
        b = ":run-shell-command build";
      };
    };
    languages = {
      language-server.pylsp.config.pylsp.plugins = {
        flake8 = {
          enabled = true;
        };
        pyls_mypy = {
          enabled = true;
          live_mode = true;
        };
        black = {
          enabled = true;
        };
      };
      # language-server.texlab.config.texlab.latexindent.modifyLineBreaks = true;
      # language-server.rust-analyzer.config.procMacro.ignored.leptos_macro = [
      #   # Optional:
      #   # "component",
      #   "server"
      # ];
      # language-server.rust-analyzer.rustfmt.overrideCommand = [
      #   "leptosfmt"
      #   "--stdin"
      #   "--rustfmt"
      # ];
      language = [
        # {
        #   name = "rust";
        #   formatter = {
        #     command = "leptosfmt";
        #     args = [
        #       "--stdin"
        #       "--rustfmt"
        #     ];
        #   };
        # }
        # Enable spellcheck for LaTeX
        {
          name = "latex";
          language-servers = [
            "texlab"
            "ltex-ls"
          ];
          indent = {
            tab-width = 4;
            unit = "    ";
          };
        }
        {
          name = "bibtex";
          indent = {
            tab-width = 4;
            unit = "    ";
            auto-format = true;
          };
        }
        # Add formatter for Python
        {
          name = "python";
          auto-format = false;
          language-servers = [ "pylsp" ];
          formatter = {
            command = "black";
            args = [ "-" ];
          };
        }
        # Add formatter for Nix
        {
          name = "nix";
          auto-format = true;
          formatter = {
            command = "nixfmt";
          };
        }

        # Fix formatting of assembly files
        {
          name = "gas";
          file-types = [
            "s"
            "S"
          ];
        }
        {
          name = "nasm";
          file-types = [
            "asm"
            "nasm"
          ];
        }
      ];
    };
  };
}
