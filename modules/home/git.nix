{ pkgs, ... }:
{
  programs.git = {
    enable = true;
    userName = "Daan Vanoverloop";
    userEmail = "daan@vanoverloop.xyz";
    signing = {
      key = "875C3A089E4D09AC";
      signByDefault = true;
    };
    delta = {
      enable = true;
      options = {
        line-numbers = true;
        side-by-side = true;
        syntax-theme = "OneHalfDark";
      };
    };
  };
}
