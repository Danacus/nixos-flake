{ pkgs, ... }:
{
  imports = [ ./gnome.nix ];

  home.packages = with pkgs; [
    rnote
    calibre
    amberol
    distrobox
    podman
    kdenlive
    ffmpeg
    python311
    thunderbird
    gst_all_1.gstreamer
    gst_all_1.gst-plugins-ugly
    gst_all_1.gst-plugins-good
    gst_all_1.gst-plugins-bad
    jellyfin-media-player
    jekyll
    bundler
    bundix
    git-lfs
    celluloid
    zotero
    xdot
    foliate
  ];

  programs.alacritty = {
    enable = true;
    settings = {
      font.size = 10.5;
      terminal.shell = "${pkgs.zsh}/bin/zsh";
      colors = {
        primary = {
          background = "#282c34";
          foreground = "#abb2bf";
        };

        normal = {
          black = "#1e2127";
          red = "#e06c75";
          green = "#98c379";
          yellow = "#d19a66";
          blue = "#61afef";
          magenta = "#c678dd";
          cyan = "#56b6c2";
          white = "#abb2bf";
        };

        bright = {
          black = "#5c6370";
          red = "#e06c75";
          green = "#98c379";
          yellow = "#d19a66";
          blue = "#61afef";
          magenta = "#c678dd";
          cyan = "#56b6c2";
          white = "#ffffff";
        };
      };
    };
  };

  programs.mpv = {
    enable = true;
    config = {
      hwdec = true;
      save-position-on-quit = true;
      osc = false;
    };
    scriptOpts = {
      osc = {
        visibility = "never";
      };
    };
    scripts = with pkgs.mpvScripts; [
      # (pkgs.callPackage ./mpv-immersive.nix { inherit buildLua; })
      uosc
      sponsorblock
    ];
  };

  services.easyeffects.enable = true;

  programs.obs-studio.enable = true;

  programs.beets = {
    enable = true;
    settings = {
      plugins = [
        "convert"
        "replaygain"
        "fetchart"
      ];
      convert = {
        copy_album_art = true;
        dest = "/home/daan/MusicLossy";
        never_convert_lossy_files = true;
        # dest = "/nas/media/daan/music_lossy";
        format = "ogg";
      };
      # convert = {
      #   copy_album_art = true;
      #   dest = "/home/daan/MusicMP3";
      #   never_convert_lossy_files = false;
      #   format = "mp3";
      # };
      replaygain = {
        auto = true;
        backend = "gstreamer";
      };
    };
  };
}
