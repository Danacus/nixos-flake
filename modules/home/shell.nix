{ pkgs, ... }: {
  programs.zsh = {
    enable = true;
    shellAliases = {
      reload = "source ~/.zshrc";
      l = "ls -l";
    };
    sessionVariables = {
      EDITOR = "hx";
      TERM = "xterm-256color";
      COLORTERM = "truecolor";
      # NIX_BUILD_SHELL = "zsh";
    };
    oh-my-zsh = {
      enable = true;
      custom = "$HOME/.config/oh-my-zsh";
      plugins = [
        "git"
        "tig"
        "gitfast"
        "colorize"
        "command-not-found"
        "cp"
        "dirhistory"
        "sudo"
      ];
    };
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; }
        { name = "zsh-users/zsh-completions"; }
        { name = "zsh-users/zsh-syntax-highlighting"; }
        { name = "zsh-users/zsh-history-substring-search"; }
        { name = "spwhitt/nix-zsh-completions"; }
        { name = "chisui/zsh-nix-shell"; }
      ];
    };
  };

  programs.nushell = {
    enable = true;
    configFile = {
      text = ''
        $env.config = {
          show_banner: false
          keybindings: [
            {
              name: trigger-completion-menu
              modifier: none
              keycode: tab
              mode: [emacs vi_normal vi_insert]
              event: { 
                until: [
                  { send: menu name: completion_menu }
                  { send: menunext }
                  { edit: complete }
                ]
              }
            }
          ]
          menus: [
            {
              name: completion_menu
              only_buffer_difference: false
              marker: $"(char -u '1f4ce') "
              type: {
                layout: columnar
                columns: 4
                col_width: 20   # Optional value. If missing all the screen width is used to calculate column width
                col_padding: 2
              }
              style: {
                text: { fg: "#33ff00" }
                selected_text: { fg: "#0c0c0c" bg: "#33ff00" attr: b}
                description_text: yellow
              }
            }
          ]
        }
      '';
    };
    envFile = {
      text = ''
        $env.EDITOR = 'hx'
        $env.TERM = 'xterm-256color';
        $env.COLORTERM = 'truecolor';
      '';
    };
  };

  programs.starship = {
    enable = true;
    settings = {
      add_newline = true;
      character = {
        success_symbol = "[➜](bold green)";
        error_symbol = "[➜](bold red)";
      };
    };
  };

  programs.ssh = {
    enable = true;
    matchBlocks = let
      cs = { host, user ? "daanvo" }: {
        inherit user;
        hostname = "${host}.cs.kuleuven.be";
        forwardAgent = true;
      };
      cs-proxy = args@{ host, user ? "daanvo" }: {
        inherit (cs args) hostname user forwardAgent;
        proxyJump = "kul-ssh";
      };
      experiments = host:
        cs-proxy {
          host = "${host}.experiments";
          user = "daan";
        };
      local = hostname: {
        user = "daan";
        hostname = "${hostname}.lan";
        forwardAgent = true;
      };
    in {
      kul-ssh = cs { host = "ssh"; };
      dnetfs = cs-proxy { host = "dnetfs"; };
      icecream = experiments "icecream";
      horus = experiments "horus";
      aeolus = experiments "aeolus";
      aexnotify = experiments "aexnotify";
      # aeolus = cs {
      #   host = "aeolus.experiments";
      #   user = "daan";
      # };
      aeolus-ilo = experiments "aeolus-ilo";
      desktop = local "danacus-desktop";
      yoga = local "danacus-yoga";
      work = local "danacus-hp";
    };
  };
}
