{ pkgs, ... }: {
  home.packages = with pkgs; [
    # bottles 
    wineWowPackages.stable
    mangohud
    protontricks
    steamtinkerlaunch
    gamescope
    jdk17
    prismlauncher
    dwarfs
    # factorio-experimental
  ];
}
