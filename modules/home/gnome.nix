{ pkgs, ... }: rec {
  imports = [ ./dconf.nix ];

  home.packages = with pkgs.gnomeExtensions; [
    gsconnect
    vitals
    caffeine
    alphabetical-app-grid
  ];

  dconf.settings = {
    # Enable installed extensions
    "org/gnome/shell".enabled-extensions =
      map (extension: extension.extensionUuid) home.packages;

    "org/gnome/shell".disabled-extensions = [ ];
  };

  gtk = {
    enable = true;
    theme = {
      # package = pkgs.gnome.gnome-themes-extra;
      # name = "Adwaita-dark";
      package = pkgs.adw-gtk3;
      name = "adw-gtk3-dark";
    };
    gtk3.extraConfig = { gtk-application-prefer-dark-theme = true; };
    gtk4.extraConfig = { gtk-application-prefer-dark-theme = true; };
  };
}
