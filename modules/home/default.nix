{ pkgs, ... }: {
  home.username = "daan";
  home.homeDirectory = "/home/daan";

  home.stateVersion = "23.05";
  programs.home-manager.enable = true;

  imports = [ ./shell.nix ./helix.nix ./git.nix ];

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    enableNushellIntegration = true;
    nix-direnv.enable = true;
  };

  services.syncthing.enable = true;
}
