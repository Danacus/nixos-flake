{
  lib,
  buildLua,
  fetchFromGitHub,
}:
buildLua {
  pname = "immersive";
  version = "1.4";

  src = fetchFromGitHub {
    owner = "Ben-Kerman";
    repo = "immersive";
    rev = "9bde59a0abaa018096de5b6f38c3ad409b9cfdf1";
    sha256 = "sha256-LrivIwPVxnYvwpQOjZ8k1iDa4T56A0bOoJVIVxCDpJ4=";
  };
}
