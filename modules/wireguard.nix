{
  lib,
  config,
  pkgs,
  ...
}:
let
  cfg = config.os.wireguard;
in
{
  options.os.wireguard = {
    ip = lib.mkOption {
      type = lib.types.str;
      default = "";
    };
  };

  config.networking.wg-quick.interfaces = lib.mkIf (cfg.ip != "") {
    home = {
      autostart = false;
      dns = [ "10.0.1.1" ];
      privateKeyFile = "/root/wireguard-keys/privatekey";
      address = [ cfg.ip ];
      peers = [
        {
          publicKey = "J00ysM9sf84FPMm7PFWNUHyM6ADb1WgXp9eZyqYWzws=";
          presharedKeyFile = "/root/wireguard-keys/psk";
          allowedIPs = [
            "10.0.0.0/16"
            "2a02:a03f:6a26:4700::1/60"
          ];
          endpoint = "ipv4_nvvqiwgyyhy0x.vanoverloop.xyz:25469";
          persistentKeepalive = 25;
        }
      ];
    };
  };
}
