{
  lib,
  config,
  pkgs,
  ...
}:
let
  cfg = config.os;
in
{
  imports = [ ./lan.nix ];

  options.os = with lib; {
    hostname = mkOption {
      type = types.str;
      default = "";
    };
    fs = {
      compression = mkOption {
        type = types.bool;
        default = false;
      };
    };
    gc = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = {
    nix.settings.experimental-features = [
      "nix-command"
      "flakes"
    ];

    # Pick only one of the below networking options.
    # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
    networking.networkmanager.enable = true; # Easiest to use and most distros use this by default.

    # Set your time zone.
    time.timeZone = "Europe/Brussels";

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Select internationalisation properties.
    i18n.defaultLocale = "en_GB.UTF-8";
    i18n.extraLocaleSettings = {
      LC_ADDRESS = "nl_BE.UTF-8";
      LC_IDENTIFICATION = "nl_BE.UTF-8";
      LC_MEASUREMENT = "nl_BE.UTF-8";
      LC_MONETARY = "nl_BE.UTF-8";
      LC_NAME = "nl_BE.UTF-8";
      LC_NUMERIC = "nl_BE.UTF-8";
      LC_PAPER = "nl_BE.UTF-8";
      LC_TELEPHONE = "nl_BE.UTF-8";
      LC_TIME = "nl_BE.UTF-8";
    };

    virtualisation.containers.enable = true;

    virtualisation = {
      docker.enable = true;
      podman = {
        enable = true;

        # Create a `docker` alias for podman, to use it as a drop-in replacement
        # dockerCompat = true;

        # Required for containers under podman-compose to be able to talk to each other.
        defaultNetwork.settings.dns_enabled = true;
      };
    };

    users.groups.home.gid = 1001;

    # Define a user account. Don't forget to set a password with 'passwd'.
    users.users.daan = {
      isNormalUser = true;
      description = "Daan Vanoverloop";
      extraGroups = [
        "networkmanager"
        "wheel"
        "home"
      ];
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7dto6irH03AR7pPbJygzYsoTb6lKRdhvo7gMPGraN+2WhsLGdPkDJCeh7kxOga9/glEGQqhH/fbePWxtSiMkqwU6DpmtGT1ynVo0eZXYxm7q/lbt7Ck6+ItehEaXM5wzm6+fzpvCq7yQMSwh3MytryDZxd95MzO6GSCN7dTPu3+/iEiHmR9CShPli8kRBTKeMOpxxE9qtQ2zACz579WI9Z8HyLQefBL0Qt3lfOe/B+xyj2xn1PgOk/9TSaKILiJkW2FSYTkBHHqfk/XQD6xt0a2kPt937vWQe/MftC/R9ZxKgNTlYhboUz06ge17AfqpLVmEJaJPCCIAd9aXl6wuwxDlISSDhPWbql5zQLZJu/IaPWazBVTMeM2KrCqaYb/6Jz5ECUT5r9w8cT2mu90yUBd7fsALfK8rufCI0PaAxa49pYY3TzIZgVSZzKEX7yBCEEKcuBUiIOTMlEyep1SqsUPCZKcRrk8yxe88Dc2xQyIT1ZjSW00zq7YZGRUbMfhkbXoCl4MmxFEi0SjX2Q+ERy97qxit1bJd6dVeqzIwrVMcYx3ufKuKjXdHIvOW03OfsTeiXN7rbjytxjweOB9PbUcWlDj8lLx8OIwca4SwDjER2T11Mqb8eQiJm228AK/Xlak7h11WzzYgp93Gnc6HrYjkIQPMlwQbZ9BPhKbuNEw== vanoverloopdaan@gmail.com"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAaaPygozb3pDMlvCH/iiqZNmR+dkgsGDhJZgki7nsqN daan@vanoverloop.xyz"
      ];
    };

    # Allow unfree packages
    nixpkgs.config.allowUnfree = true;

    # List packages installed in system profile. To search, run:
    # $ nix search wget
    environment.systemPackages = with pkgs; [
      vim
      helix
      git
      eid-mw
      pcsctools
      scrcpy
      nil
      nixfmt-rfc-style
      htop
    ];

    services.udev.packages = [ pkgs.game-devices-udev-rules ];

    # List services that you want to enable:

    # Enable the OpenSSH daemon.
    services.openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
      };
    };

    environment.sessionVariables = {
      XDG_CACHE_HOME = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_DATA_HOME = "$HOME/.local/share";
      XDG_STATE_HOME = "$HOME/.local/state";

      # HACK: winit is broken and requires this to be set to work properly
      XCURSOR_THEME = "Adwaita";
    };

    nix.extraOptions = ''
      min-free = ${toString (2048 * 1024 * 1024)}
      max-free = ${toString (4096 * 1024 * 1024)}
    '';

    networking.hostName = cfg.hostname;

    # Enable compression
    fileSystems = lib.mkIf (cfg.fs.compression) {
      "/".options = [ "compress=zstd" ];
      "/home".options = [ "compress=zstd" ];
      "/nix".options = [
        "compress=zstd"
        "noatime"
      ];
    };

    nix.gc = lib.mkIf (cfg.gc) {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 15d";
    };

    services.fwupd.enable = true;

    # Syncthing ports
    networking.firewall.allowedTCPPorts = [
      8384
      22000
    ];
    networking.firewall.allowedUDPPorts = [
      22000
      21027
    ];

    # Open ports in the firewall.
    # networking.firewall.allowedTCPPorts = [ 8080 ];
    # networking.firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # networking.firewall.enable = false;

    # Copy the NixOS configuration file and link it from the resulting system
    # (/run/current-system/configuration.nix). This is useful in case you
    # accidentally delete configuration.nix.
    # system.copySystemConfiguration = true;

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It's perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "23.05"; # Did you read the comment?
  };
}
