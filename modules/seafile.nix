{ lib, config, pkgs, ... }: {
  services.seafile = {
    enable = true;
    adminEmail = "admin@vanoverloop.xyz";
    initialAdminPassword = "admin";
    ccnetSettings.General.SERVICE_URL = "https://files.vanoverloop.xyz";
  };

  services.nginx = {
    enable = true;
    virtualHosts."seafile" = {
      listen = [{
        addr = "0.0.0.0";
        port = 8527;
      }];
      locations."/" = {
        proxyPass = "http://unix:/run/seahub/gunicorn.sock";
        proxyWebsockets = true;
      };
    };
  };

  # Make sure nginx has access to the gunicorn socket
  users.users.nginx.extraGroups = [ "seafile" ];

  networking.firewall.allowedTCPPorts = [ 8527 ];
}
