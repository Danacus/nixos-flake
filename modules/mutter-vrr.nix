{ config, pkgs, ... }: {
  nixpkgs.overlays = [
    (self: super: {
      gnome = super.gnome.overrideScope' (pself: psuper: {
        mutter = psuper.mutter.overrideAttrs (oldAttrs: {
          patches = (oldAttrs.patches or [ ]) ++ [
            (super.fetchpatch {
              url =
                "https://raw.githubusercontent.com/KyleGospo/gnome-vrr/main/mutter/vrr.patch";
              hash = "sha256-Wl8sGPDm7RoY6j7DehpV3YR6INvUzuuk3gBrzXsYWjU=";
            })
          ];
        });

        gnome-control-center = psuper.gnome-control-center.overrideAttrs
          (oldAttrs: {
            patches = (oldAttrs.patches or [ ]) ++ [
              (super.fetchpatch {
                url =
                  "https://raw.githubusercontent.com/KyleGospo/gnome-vrr/main/gnome-control-center/734.patch";
                hash = "sha256-jLmee+4eHMWYd0MC3+dRRLmPQgHj/yB8WEFPIDCQODE=";
              })
            ];
          });
      });
    })
  ];

  services.xserver.desktopManager.gnome = {
    extraGSettingsOverrides = ''
      [org.gnome.mutter]
      experimental-features=['variable-refresh-rate']
    '';

    extraGSettingsOverridePackages = [ pkgs.gnome.mutter ];
  };

  environment.sessionVariables = {
    # Fix VRR cursor stutter
    MUTTER_DEBUG_DISABLE_HW_CURSORS = "1";
  };
}
