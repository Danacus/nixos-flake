{ lib, config, pkgs, ... }:
let cfg = config.os.upgrade;
in {
  options.os.upgrade = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
    reboot = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
    repo = lib.mkOption {
      type = lib.types.str;
      default = "git+https://gitlab.com/Danacus/nixos-flake";
    };
    name = lib.mkOption {
      type = lib.types.str;
      default = ".";
    };
  };

  config.system.autoUpgrade = {
    enable = cfg.enable;
    flake = "${cfg.repo}#${cfg.name}";
    flags = [ "--refresh" ];
    dates = "03:00";
    rebootWindow = {
      lower = "01:00";
      upper = "05:00";
    };
    allowReboot = cfg.reboot;
  };

  config.systemd.services.nixos-upgrade.preStart =
    "${pkgs.wget}/bin/wget -q --spider https://gitlab.com/Danacus/nixos-flake";
}
