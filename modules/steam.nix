{ pkgs, ... }:
let
  gamescopeEnv = {
    # https://github.com/ChimeraOS/gamescope-session-steam/blob/main/usr/share/gamescope-session-plus/sessions.d/steam
    STEAM_GAMESCOPE_VRR_SUPPORTED = "1";
    STEAM_MANGOAPP_PRESETS_SUPPORTED = "1";
    STEAM_USE_MANGOAPP = "1";
    STEAM_USE_DYNAMIC_VRS = "1";
    STEAM_GAMESCOPE_HAS_TEARING_SUPPORT = "1";
    STEAM_GAMESCOPE_TEARING_SUPPORTED = "1";
    STEAM_GAMESCOPE_HDR_SUPPORTED = "1";
    STEAM_ENABLE_VOLUME_HANDLER = "1";
    SRT_URLOPEN_PREFER_STEAM = "1";
    STEAM_DISABLE_AUDIO_DEVICE_SWITCHING = "1";
    # STEAM_MULTIPLE_XWAYLANDS = "1";
    STEAM_GAMESCOPE_DYNAMIC_FPSLIMITER = "1";
    STEAM_DISABLE_MANGOAPP_ATOM_WORKAROUND = "1";
    STEAM_MANGOAPP_HORIZONTAL_SUPPORTED = "1";
    STEAM_GAMESCOPE_FANCY_SCALING_SUPPORT = "1";
    STEAM_GAMESCOPE_COLOR_MANAGED = "1";
    STEAM_GAMESCOPE_VIRTUAL_WHITE = "1";
  };
  gamescopeArgs = [
    "-w"
    "3440"
    "-h"
    "1440"
    "-W"
    "3440"
    "-H"
    "1440"
    "--adaptive-sync"
    "--framerate-limit"
    "144"
    "--hdr-enabled"
  ];
  steamArgs = [
    "-gamepadui"
    "-steamos3"
    "-tenfoot"
    "-pipewire-dmabuf"
    #"-steampal" 
    #"-steamdeck"
  ];
  steam-gamescope = let
    exports = builtins.attrValues
      (builtins.mapAttrs (n: v: "export ${n}=${v}") gamescopeEnv);
  in pkgs.writeShellScriptBin "steam-gamescope" ''
    ${builtins.concatStringsSep "\n" exports}
    gamescope --steam ${toString gamescopeArgs} -- steam ${toString steamArgs}
  '';

  gamescopeSessionFile =
    (pkgs.writeTextDir "share/wayland-sessions/steam.desktop" ''
      [Desktop Entry]
      Name=Steam
      Comment=A digital distribution platform
      Exec=${steam-gamescope}/bin/steam-gamescope
      Type=Application
    '').overrideAttrs (_: { passthru.providedSessions = [ "steam" ]; });
in {
  programs.steam = {
    enable = true;
    package = pkgs.steam.override {
      extraPkgs = pkgs:
        with pkgs; [
          xorg.libXcursor
          xorg.libXi
          xorg.libXinerama
          xorg.libXScrnSaver
          libpng
          libpulseaudio
          libvorbis
          stdenv.cc.cc.lib
          libkrb5
          keyutils
          mono
          mono4
          mono5
          fuse
          fuse3
        ];
    };
    remotePlay.openFirewall =
      true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall =
      true; # Open ports in the firewall for Source Dedicated Server
  };

  networking.firewall.allowedTCPPorts = [ 27040 ];
  networking.firewall.allowedUDPPorts = [ 27031 27032 27033 27034 27035 27036 ];

  programs.gamescope.enable = true;
  environment.systemPackages =
    [ steam-gamescope pkgs.mangohud pkgs.protontricks ];
  services.displayManager.sessionPackages = [ gamescopeSessionFile ];
}
