{ lib, config, pkgs, ... }: {
  options.os = with lib; {
    network = {
      domain = mkOption { type = types.str; };
      services = mkOption {
        type = with types;
          attrsOf (submodule {
            options = {
              endpoint = mkOption { type = str; };
              host = mkOption { type = str; };
              port = mkOption { type = int; };
              external = mkOption {
                type = bool;
                default = true;
              };
            };
          });
      };
    };
  };

  config = {
    os.network = {
      domain = "vanoverloop.xyz";
      services = let
        nasService = { endpoint, port, external ? true }: {
          inherit endpoint port external;
          host = "nas.lan";
        };
        internalNasService = endpoint: port:
          nasService {
            inherit endpoint port;
            external = false;
          };
        externalNasService = endpoint: port:
          nasService {
            inherit endpoint port;
            external = true;
          };
        piService = endpoint: port: {
          inherit endpoint port;
          host = "pi.lan";
        };
      in {
        qbittorrent = internalNasService "torrent" 8080;
        radarr = internalNasService "movies" 7878;
        sonarr = internalNasService "series" 8989;
        prowlarr = internalNasService "prowlarr" 9696;
        home-assistant = piService "hass" 8123;
        vaultwarden = piService "vault" 8485;
        gitea = piService "git" 8444;
        keycloak = externalNasService "auth" 8658;
        nextcloud = externalNasService "cloud" 8649;
        jellyfin = externalNasService "jellyfin" 8096;
      };
    };
  };
}
