{
  lib,
  config,
  pkgs,
  ...
}:
let
  port = 8658;
  sslPort = 8659;
in
{
  services.keycloak = {
    enable = true;
    package = pkgs.keycloak.overrideAttrs (
      final: prev: { patches = prev.patches ++ [ ./keycloak-zip-disablememorymapping.patch ]; }
    );
    database = {
      type = "postgresql";
      passwordFile = "/root/keycloak_db_password";
    };
    settings = {
      # hostname-url = "http://localhost:8658";
      hostname = "auth.vanoverloop.xyz";
      proxy-headers = "forwarded|xforwarded";
      http-enabled = true;
      http-port = port;
      https-port = sslPort;
    };
  };

  systemd.services.keycloak.serviceConfig.TimeoutStartSec = 1800;

  services.postgresql.settings.standard_conforming_strings = true;
  networking.firewall.allowedTCPPorts = [
    port
    sslPort # Keycloak
    389
    636 # ldap and ldaps
  ];

  services.openldap = {
    enable = true;
    settings = {
      attrs = {
        objectClass = "olcGlobal";
        cn = "config";
      };
      children = {
        "cn=schema" = {
          includes = [
            "${pkgs.openldap}/etc/schema/core.ldif"
            "${pkgs.openldap}/etc/schema/cosine.ldif"
            "${pkgs.openldap}/etc/schema/inetorgperson.ldif"
            "${pkgs.openldap}/etc/schema/nis.ldif"
          ];
          attrs = {
            objectClass = "olcSchemaConfig";
            cn = "schema";
          };
        };
        "cn=module{0}" = {
          attrs = {
            objectClass = "olcModuleList";
            cn = "module{0}";
            olcModulePath = "${pkgs.openldap}/lib/modules";
            olcModuleLoad = [
              "{0}back_mdb.la"
              "{1}memberof.la"
            ];
          };
        };
        "olcDatabase={-1}frontend" = {
          attrs = {
            objectClass = [
              "olcDatabaseConfig"
              "olcFrontendConfig"
            ];
            structuralObjectClass = "olcDatabaseConfig";
            olcDatabase = "{-1}frontend";
          };
        };
        "olcDatabase={0}config" = {
          attrs = {
            objectClass = "olcDatabaseConfig";
            olcDatabase = "{0}config";
            olcAccess = [ "{0}to * by * none" ];
            olcRootDN = "cn=config";
          };
        };
        "olcDatabase={1}mdb" = {
          children."olcOverlay={0}memberof" = {
            attrs = {
              objectClass = [
                "olcOverlayConfig"
                "olcMemberOf"
              ];
              structuralObjectClass = "olcMemberOfConfig";
              olcMemberOfDangling = "ignore";
              olcMemberOfRefInt = "TRUE";
              olcMemberOfGroupOC = "groupOfUniqueNames";
              olcMemberOfMemberAD = "uniqueMember";
              olcMemberOfMemberOfAD = "memberOf";
            };
          };
          attrs = {
            objectClass = [
              "olcDatabaseConfig"
              "olcMdbConfig"
            ];
            olcDatabase = "{1}mdb";
            olcDbDirectory = "/var/lib/openldap/ldap";
            olcDbIndex = [ "objectClass eq" ];
            olcSuffix = "dc=vanoverloop,dc=xyz";
            olcRootDN = "cn=admin,dc=vanoverloop,dc=xyz";
            olcRootPW = "{SSHA}23qqu1euMBQF8OU9Gc/n/iSlGW7cNoys";
          };
        };
        "olcDatabase={2}monitor" = {
          attrs = {
            objectClass = "olcDatabaseConfig";
            olcDatabase = "{2}monitor";
            olcRootDN = "cn=config";
          };
        };
      };
    };
  };
}
