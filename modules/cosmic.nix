{
  config,
  pkgs,
  lib,
  ...
}:
{
  services.desktopManager.cosmic.enable = true;
  services.displayManager.cosmic-greeter.enable = true;
  services.xserver.displayManager.gdm.enable = lib.mkForce false;
}
