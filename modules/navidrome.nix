{ lib, config, pkgs, ... }: {
  services.navidrome = {
    enable = true;
    openFirewall = true;
    settings = {
      MusicFolder = "/nas/media/daan/music";
      Address = "0.0.0.0";
      Port = 4533;
    };
  };
}
