{ lib, config, pkgs, ... }: {
  services.jellyfin = {
    enable = true;
    user = "jellyfin";
    group = "home";
    openFirewall = true;
  };
  systemd.services.jellyfin.serviceConfig = { Restart = lib.mkForce "always"; };
}
