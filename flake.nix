{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    aagl.url = "github:ezKEa/aagl-gtk-on-nix/release-24.11";
    aagl.inputs.nixpkgs.follows = "nixpkgs";
    nixpkgs.follows = "nixos-cosmic/nixpkgs-stable";
    nixos-cosmic.url = "github:lilyinstarlight/nixos-cosmic";
    # nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    # chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";
  };
  outputs =
    {
      self,
      nixpkgs,
      home-manager,
      aagl,
      nixos-cosmic,
    }:
    let
      lib = nixpkgs.lib;

      common = {
        system = map (path: ./modules/${path}) [
          "common.nix"
          "auto-upgrade.nix"
          "nix-channel.nix"
          "wireguard.nix"
        ];
        home = [ ./modules/home ];
      };

      common-desktop-modules = {
        system =
          common.system
          ++ map (path: ./modules/${path}) [
            "boot-efi.nix"
            "desktop.nix"
            "ccache.nix"
            "nasnfs.nix"
          ];
        home = common.home ++ map (path: ./modules/home/${path}) [ "desktop.nix" ];
      };

      common-server-modules = {
        system = common.system;
        home = common.home;
      };

      home-manager-modules = home-manager: user-config: [
        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.daan = user-config;
        }
      ];

      modules =
        commonModules: name:
        {
          extraSystem ? [ ],
          extraHome ? [ ],
        }:
        commonModules.system
        ++ extraSystem
        ++ [ ./systems/${name}/configuration.nix ]
        ++ home-manager-modules home-manager { imports = commonModules.home ++ extraHome; };

      desktop-modules = modules common-desktop-modules;
      server-modules = modules common-server-modules;
    in
    {
      nixosConfigurations.danacus-yoga = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = desktop-modules "yoga" {
          extraSystem = [
            # ./modules/auth.nix
            {
              os = {
                wireguard.ip = "10.0.7.3/32";
                fs.compression = true;
                hostname = "danacus-yoga";
                upgrade = {
                  enable = true;
                  name = "danacus-yoga";
                };
              };
            }
          ];
        };
      };
      nixosConfigurations.danacus-hp1 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = desktop-modules "hp1" {
          extraSystem = [
            ./modules/corectrl.nix
            {
              os = {
                wireguard.ip = "10.0.7.8/32";
                fs.compression = true;
                hostname = "danacus-hp";
                upgrade = {
                  enable = true;
                  name = "danacus-hp1";
                };
              };
            }
          ];
        };
      };
      nixosConfigurations.danacus-hp2 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = desktop-modules "hp2" {
          extraSystem = [
            ./modules/corectrl.nix
            ./modules/steam.nix
            {
              os = {
                wireguard.ip = "10.0.7.3/32";
                fs.compression = true;
                hostname = "danacus-hp";
                upgrade = {
                  enable = true;
                  name = "danacus-hp2";
                };
              };
            }
          ];
        };
      };
      nixosConfigurations.danacus-desktop = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = desktop-modules "desktop" {
          extraSystem = [
            # ./modules/kde.nix
            ./modules/steam.nix
            ./modules/corectrl.nix
            # ./modules/k3s.nix
            # ./modules/mutter-vrr.nix
            # ./modules/nextcloud.nix
            # ./modules/auth.nix
            # ./modules/torrent.nix
            # ./modules/proxy.nix
            # ./modules/navidrome.nix
            # {
            #   nix.settings = {
            #     substituters = [ "https://cosmic.cachix.org/" ];
            #     trusted-public-keys = [ "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE=" ];
            #   };
            # }
            # nixos-cosmic.nixosModules.default
            # ./modules/cosmic.nix
            {
              imports = [ aagl.nixosModules.default ];
              nix.settings = aagl.nixConfig;
              programs.anime-game-launcher.enable = true;
            }
            # chaotic.nixosModules.default
            # { chaotic.hdr.enable = true; }
            {
              os = {
                wireguard.ip = "10.0.7.2/32";
                fs.compression = true;
                hostname = "danacus-desktop";
                upgrade = {
                  enable = true;
                  name = "danacus-desktop";
                };
              };
            }
          ];
          extraHome = [ ./modules/home/gaming.nix ];
        };
      };
      nixosConfigurations.nas = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = server-modules "nas" {
          extraSystem = [
            ./modules/boot-efi.nix
            ./modules/nasnfs.nix
            ./modules/jellyfin.nix
            ./modules/torrent.nix
            ./modules/auth.nix
            ./modules/nextcloud.nix
            # ./modules/seafile.nix
            {
              users.users.daan.linger = true;
              os = {
                hostname = "nas";
                upgrade = {
                  enable = true;
                  name = "nas";
                  reboot = false;
                };
                gc = true;
              };
            }
          ];
        };
      };
      nixosConfigurations.pi = nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        modules = server-modules "pi" {
          extraSystem = [
            # nixos-hardware.nixosModules.raspberry-pi-4
            {
              os = {
                hostname = "pi";
                upgrade = {
                  enable = true;
                  name = "pi";
                  reboot = true;
                };
                gc = true;
              };
            }
          ];
        };
      };
    };
}
