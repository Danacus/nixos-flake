{ config, pkgs, ... }: {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Intel GPU only: enable VAAPI
  hardware.opengl.extraPackages = with pkgs; [ intel-media-driver ];
}

