{
  config,
  pkgs,
  lib,
  ...
}:
{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Open DHCP ports
  networking.firewall.allowedUDPPorts = [
    67
    68
  ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.kernelPatches = [
    # {
    #   name = "platform-x86-amd-pmc-Add-10ms-delay-to-2nd-s0i3-cycl";
    #   patch = ./0001-platform-x86-amd-pmc-Add-10ms-delay-to-2nd-s0i3-cycl.patch;
    # }
    # {
    #   name = "amd-debug";
    #   patch = ./0002-amd-debug.patch;
    # }
  ];

  services.udev.packages = [ pkgs.dolphin-emu ];
  environment.systemPackages = [ pkgs.dolphin-emu ];

  services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTRS{idVendor}=="0a5c", ATTRS{idProduct}=="21e8", TAG+="uaccess" 
  '';
}
