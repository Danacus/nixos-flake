{ config, pkgs, ... }: {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Mount devices
  fileSystems."/storage" = {
    device = "/dev/disk/by-label/Storage";
    fsType = "btrfs";
    options = [ "compress=zstd" "x-systemd.automount" ];
  };

  fileSystems."/more_storage" = {
    device = "/dev/disk/by-label/MoreStorage";
    fsType = "ext4";
    options = [ "x-systemd.automount" ];
  };

  fileSystems."/old_storage" = {
    device = "/dev/disk/by-label/OldStorage";
    fsType = "ext4";
    options = [ "x-systemd.automount" ];
  };

  # Might fix bluetooth dongle
  hardware.enableAllFirmware = true;

  boot.loader.systemd-boot.memtest86.enable = true;

  boot.kernel.sysctl = {
    # Ubisoft Connect needs this
    "net.ipv4.tcp_mtu_probing" = 1;
  };

  # boot.kernelPackages = pkgs.linuxPackages;

  networking.firewall.allowedTCPPorts = [ 1234 ];

  services.udev.packages = [ pkgs.dolphin-emu ];
  environment.systemPackages = [ pkgs.dolphin-emu ];

  services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTRS{idVendor}=="0a5c", ATTRS{idProduct}=="21e8", TAG+="uaccess" 
  '';

  services.upower.package = pkgs.upower.overrideAttrs
    (final: prev: { patches = prev.patches ++ [ ./upower-ds4.patch ]; });
}

