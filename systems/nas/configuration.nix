{ lib, config, pkgs, ... }: {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # swapDevices = lib.mkForce [ ];

  # qBittorrent uses a lot of files mapped to memory, resulting into a massive address space.
  # Since qBittorrent is more active than pretty much all other programs, the anonymous pages
  # from other programs will be swapped to disk in favor of the file cache for qBittorrent.
  # This has the unfortunate consequence that all other programs will be slow and unresponsive
  # when they are used again, because all their anonymous pages need to be swapped in again.
  # In an attempt to mitigate this issue, swappiness is set to 0, which will prevent
  # anonymous pages from being evicted in favour of the file cache.
  boot.kernel.sysctl."vm.swappiness" = 0;
}

