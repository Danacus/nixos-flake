{ config, pkgs, lib, ... }: {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Intel GPU only: enable VAAPI
  hardware.opengl.extraPackages = with pkgs; [ intel-media-driver ];

  # Open DHCP ports
  networking.firewall.allowedUDPPorts = [ 67 68 ];
}

